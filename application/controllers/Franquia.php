<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Franquia extends CI_Controller {



	public function __construct() {
        
        parent::__construct();

        #use smartyCI template .tpl
        $this->load->library('smartyciclass');
        $this->load->library('pagination');
        $this->load->service('paginator_service');
        $this->load->helper('url');
        $this->load->service('franquia_service');
        $this->load->helper('serviceresponse');


        
    }


    public function add(){

        $data = array(

            "id_chamado" => $_REQUEST['chamado'],
            "id_exp"     => $_REQUEST['id_exp'],
            "percentual" => $_REQUEST['franquia'],
            "relacao"    => $_REQUEST['relacao'],
            "opcao"      => $_REQUEST['opcao']
        );



        $responseAdd = $this->franquia_service->add($data);



        echo json_encode($responseAdd); 

    }

	public function show()
	{
        #--------------------GET Pagination List
        $data = array(
            'url'   => "",
            'list'  => null,
            'page'  => 5 ,
        );


        #-------------------------------------------GET LIST Pagination
        $list  = $this->paginator_service->pagination($data);


        $this->smartyciclass->display('template/header.tpl');     
        $this->smartyciclass->display('franquia/franquia.tpl',$list); 
        $this->smartyciclass->display('template/footer.tpl'); 




    }

   public function listFranquia(){

        #GET FIELDS
        $id_exp     = $_REQUEST['id'];
        $id_chamado = $_REQUEST['id_chamado'];


        $id_chamado = "Correção de fase - ".$id_chamado;

        #VERIFY SE EXISTS FRANQUIA
        $data = array(
            "sql"    => "SELECT * FROM sis_produtos_franquia_config  WHERE id_exp={$id_exp}"
        );

        #list
        $responseFranquia = $this->franquia_service->listFranquia($data);

        
        #case exist franquia into    
        if(empty($responseFranquia["success"])){

            $data = array(
                "sql"  => "SELECT  ce.id_relacao , s.id_exp, s.cod_estipulante, s.id_produto, s.id_cobertura, s.cod_prestacao  FROM sis_exp s,sis_clientes_estipulantes ce WHERE s.id_exp={$id_exp} and s.cod_estipulante = ce.cod_estipulante;"         
            );

            

            #GET LIST PRINCIPAL ID_RELACAO
            $responseFranquia = $this->franquia_service->listFranquia($data);            


           
            #--------------------GET Pagination List
            $dataArray = array(
                    'url'   => "",
                    'list'  => $responseFranquia,
                    'page'  => 5 ,
            ); 


        }else{

            $msqSql = "SELECT * FROM sis_produtos_franquia_config  WHERE id_exp in ($id_exp)";

            if(!isset($responseFranquia["id_relacao"])){
                $dataArray = array(
                  "list" => getServiceResponse(FALSE,"Franquia sem porcentagem definida","erro",$responseFranquia),
                );

            }else if(isset($responseFranquia["cod_prestacao"])){

                if($responseFranquia["cod_prestacao"]=="" || $responseFranquia["cod_prestacao"]==NULL){

                    $dataArray = array(
                      "list" => getServiceResponse(FALSE,"Insira o código de Prestação na tabela nessa Oss com o seguinte sql - $msqSql ","erro",$responseFranquia),
                    );
                }                

            }else if(isset($responseFranquia["id_estipulante"])) {

                if($responseFranquia["id_estipulante"]=="" || $responseFranquia["id_estipulante"]==NULL){

                    $dataArray = array(
                      "list" => getServiceResponse(FALSE,"Insira o Id do Estipulante na tabela nessa Oss com o seguinte sql - $msqSql ","erro",$responseFranquia),
                    );
                } 
            }else if(isset($responseFranquia["id_relacao"])){

                $dataArray = array(
                    'url'   => "",
                    'list'  => $responseFranquia,
                    'page'  => 5 ,
                );

                #-------------------------------------------GET LIST Pagination
                $list  = $this->paginator_service->pagination($dataArray);

            }

        }

        echo json_encode($dataArray); 
   } 
        


}    