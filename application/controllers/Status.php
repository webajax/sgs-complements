
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Status extends CI_Controller {



	public function __construct() {
        
        parent::__construct();

        #use smartyCI template .tpl
        $this->load->library('smartyciclass');
        $this->load->library('pagination');
        $this->load->service('paginator_service');
        $this->load->helper('url');
        $this->load->service('status_service');
        
    }


    public function update(){


        $id         = $_REQUEST['id'];
        $id_chamado = $_REQUEST['id_chamado'];
        $id_exp     = $_REQUEST['id_exp'];


        $data = array(
            "id_exp" => $id_exp,
        );



        $responseAdd = $this->status_service->update($data);

        echo json_encode($responseAdd); 

    }

	public function show()
	{
        #--------------------GET Pagination List
        $data = array(
            'url'   => "",
            'list'  => null,
            'page'  => 5 ,
        );


        #-------------------------------------------GET LIST Pagination
        $list  = $this->paginator_service->pagination($data);


        $this->smartyciclass->display('template/header.tpl');     
        $this->smartyciclass->display('status/status.tpl',$list); 
        $this->smartyciclass->display('template/footer.tpl'); 




    }

   public function listStatus(){

        $id_exp     = $_REQUEST['id'];
        $id_chamado = $_REQUEST['id_chamado'];

        $id_chamado = "Correção de analise - ".$id_chamado;

        $data = array(
            "id_exp"     => $id_exp,
        );

        $responsestatus = $this->status_service->liststatus($data);


        if($responsestatus["success"]){

            #--------------------GET Pagination List
            $data = array(
                'url'   => "",
                'list'  => $responsestatus,
                'page'  => 5 ,
            );


            #-------------------------------------------GET LIST Pagination
            $list          = $this->paginator_service->pagination($data);


        }else{

            #--------------------GET Pagination List
            $data = array(
                'url'   => "",
                'list'  => $responsestatus,
                'page'  => 5 ,
            );


            #-------------------------------------------GET LIST Pagination
            $list          = $this->paginator_service->pagination($data);



        }


          echo json_encode($list); 

   } 
        


}    