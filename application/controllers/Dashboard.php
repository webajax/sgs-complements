<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {



	public function __construct() {
        
        parent::__construct();

        #use smartyCI template .tpl
        $this->load->library('smartyciclass');
        $this->load->helper('url');

        
    }

	public function index()
	{



        $this->smartyciclass->display('template/header.tpl');     
        $this->smartyciclass->display('welcome/welcome.tpl'); 
        $this->smartyciclass->display('template/footer.tpl'); 




    }

}    