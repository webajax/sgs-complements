<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fase extends CI_Controller {



	public function __construct() {
        
        parent::__construct();

        #use smartyCI template .tpl
        $this->load->library('smartyciclass');
        $this->load->library('pagination');
        $this->load->service('paginator_service');
        $this->load->helper('url');
        $this->load->service('fase_service');


        
    }


    public function add(){


        $id         = $_REQUEST['id'];
        $id_chamado = $_REQUEST['id_chamado'];
        $id_exp     = $_REQUEST['id_exp'];


        $data = array(
            "id_exp"          => $id_exp,
            "id_fases_status" => $id,
            "descricao"       => 'Tipo de fase/status errados',
            "status"          => 'S',
            "dtdatalog"       => date('Y-m-d h:m:s'),
            "inidusuariolog"  => 13323,
            "vcmotivolog"     => $id_chamado
        );


        $responseAdd = $this->fase_service->add($data);

        echo json_encode($responseAdd); 

    }

	public function show()
	{
        #--------------------GET Pagination List
        $data = array(
            'url'   => "",
            'list'  => null,
            'page'  => 5 ,
        );


        #-------------------------------------------GET LIST Pagination
        $list  = $this->paginator_service->pagination($data);


        $this->smartyciclass->display('template/header.tpl');     
        $this->smartyciclass->display('fase/fase.tpl',$list); 
        $this->smartyciclass->display('template/footer.tpl'); 




    }

   public function listFase(){

        $id_exp     = $_REQUEST['id'];
        $id_chamado = $_REQUEST['id_chamado'];

        $id_chamado = "Correção de fase - ".$id_chamado;

        $data = array(
            "id_exp"     => $id_exp,
        );

        $responseFase = $this->fase_service->listFase($data);

        if($responseFase["success"]){

        #--------------------GET Pagination List
        $data = array(
            'url'   => "",
            'list'  => $responseFase,
            'page'  => 5 ,
        );


        #-------------------------------------------GET LIST Pagination
        $list          = $this->paginator_service->pagination($data);



         echo json_encode($list); 
 

        }

   } 
        


}    