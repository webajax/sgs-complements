<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



 	function getServiceResponse($success,$message,$redirect,$custom) {
    	return array(
        	'success'   => $success,
        	'message'   => $message,
        	'redirect'  => $redirect,
        	'custom'    => $custom
    );
	
    }	
