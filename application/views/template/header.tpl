<!DOCTYPE html>
<html>
<head>

 
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!--<meta http-equiv="content-type" content="text/html; charset=utf-8">-->
    <title>Sgs Tools</title>


  	<link href="{base_url('') }css/bootstrap.min.css" rel="stylesheet">
    <link href="{base_url('') }css/bootstrap.css" rel="stylesheet">
    <link href="{base_url('') }font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="{base_url('') }css/animate.css" rel="stylesheet">
    <link href="{base_url('') }css/style.css" rel="stylesheet">    
    <link href="{base_url('') }css/basic.css" rel="stylesheet">     <!-- Usado pela area de upload de arquivos -->
    <link href="{base_url('') }css/dropzone.css" rel="stylesheet">  <!-- Usado pela area de upload de arquivos -->
    <link href="{base_url('') }css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="{base_url('') }css/plugins/chosen/chosen.css" rel="stylesheet">
    <link href="{base_url('') }css/plugins/colorpicker/bootstrap-colorpicker.min.css" rel="stylesheet">
    <link href="{base_url('') }css/plugins/cropper/cropper.min.css" rel="stylesheet">
    <link href="{base_url('') }css/plugins/switchery/switchery.css" rel="stylesheet">
    <link href="{base_url('') }css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
    <link href="{base_url('') }css/plugins/nouslider/jquery.nouislider.css" rel="stylesheet">
    <link href="{base_url('') }css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    <link href="{base_url('') }css/plugins/ionRangeSlider/ion.rangeSlider.css" rel="stylesheet">
    <link href="{base_url('') }css/plugins/ionRangeSlider/ion.rangeSlider.skinFlat.css" rel="stylesheet">
 	<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <link href="{base_url('') }css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
    <link href="{base_url('') }css/plugins/dataTables/dataTables.responsive.css" rel="stylesheet">
    <link href="{base_url('') }css/plugins/dataTables/dataTables.tableTools.min.css" rel="stylesheet">
	<link href="{base_url('') }css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
    <link href="{base_url('') }css/custom.css" rel="stylesheet">
    <link href="{base_url('') }css/select2.css" rel="stylesheet"> 

    <!--layers css-->
    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{base_url('') }css/jqcloud/jqcloud.css" />   


    <!--datepicker JS-->
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css" />
    <script src="http://code.jquery.com/jquery-1.8.3.js"></script>
    <script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script> 



    <!-- Mainly scripts -->
    <script src="{base_url('') }js/jquery-2.1.1.js"></script>
    <script src="{base_url('') }js/bootstrap.min.js"></script>
    <script src="{base_url('') }js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="{base_url('') }js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="{base_url('') }js/plugins/jeditable/jquery.jeditable.js"></script>
    <!-- Custom and plugin javascript -->
    <script src="{base_url('') }js/inspinia.js"></script>
    <script src="{base_url('') }js/plugins/pace/pace.min.js"></script>
    <!-- Flot -->
    <script src="{base_url('') }js/plugins/flot/jquery.flot.js"></script>
    <script src="{base_url('') }js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="{base_url('') }js/plugins/flot/jquery.flot.resize.js"></script>
    <!-- JSKnob -->
    <script src="{base_url('') }js/plugins/jsKnob/jquery.knob.js"></script>
    <!-- ChartJS-->
    <script src="{base_url('') }js/plugins/chartJs/Chart.min.js"></script>
    <!-- Peity -->
    <script src="{base_url('') }js/plugins/peity/jquery.peity.min.js"></script>
    <!-- Peity demo -->
    <script src="{base_url('') }js/demo/peity-demo.js"></script>
    <!-- Toggle script -->

    <script src="{base_url('') }js/plugins/dataTables/jquery.dataTables.js"></script>

    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>


    <!--select 2-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>


    <!--jAlert-->
    <link href="{base_url('') }css/jAlert.css" rel="stylesheet"> 
    <script type="text/javascript" src="{base_url('') }js/jAlert.js"></script>
    <script type="text/javascript" src="{base_url('') }js/jAlert-functions.js"></script>



    <!--layers-->
    <script type="text/javascript" src="{base_url('') }js/jqcloud/jqcloud-1.0.4.js"></script>


</head>
<body class="top-navigation">


    <div id="wrapper">
        <div id="page-wrapper" class="blue-bg">
          <div class="row border-bottom white-bg">
            <nav class="navbar navbar-fixed-top" role="navigation">
                <div class="navbar-header">
                    <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                        <i class="fa fa-reorder"></i>
                    </button>
                    <a href="{base_url('welcome') }" class="navbar-brand" text-font-bold="true">
                            <img src="{base_url('img/banner_sgs_logo_topo.jpg') }" style="position: relative;left: -50px" width="250px" height="40px">
                            </a>

                </div>
                <div class="navbar-collapse collapse" id="navbar" style="position: relative;left: -100px" >
                  <ul class="nav navbar-nav">
                    <li class="dropdown">
                      <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown"> Consultas <span class="caret"></span></a>
                      <ul role="menu" class="dropdown-menu">
                        <li>
                            <a href="{base_url('fase/show') }">Alteração de Fase</a>
                        </li>
                        <li>
                            <a href="{base_url('franquia/show') }">Alteração de Franquia %</a>
                        </li>
                        <li>
                            <a href="{base_url('analise/show') }">Exibir Botão Análise</a>
                        </li>
                        <li>
                            <a href="{base_url('status/show') }">Alterar Status para Aprovado</a>
                        </li>
                        <li>
                            <a href="{base_url('documento/show') }">Incluir Documento</a>
                        </li>
                      </ul>
                    </li>

                    <li class=" " >
                      <a aria-expanded="false" role="button" href="#"> Ajuda </a>
                    </li>
                    <li >


                    </li>
                  </ul>

                  <!--menu carga-->
 
                 
                  <!--end carga menu-->         

                    <ul class="nav navbar-top-links navbar-right">
                        <li>
                          <a href="{base_url('user/logout') }">
                            <i class="fa fa-sign-out"></i> Logout
                          </a>
                        </li>
                    </ul>
                </div>
            </nav>
          </div>

