        

<input type="hidden" name="base_url_list" id="base_url_list" value="{base_url('fase/listFase') }">
<input type="hidden" name="base_url_add" id="base_url_add" value="{base_url('fase/add') }">

        </br>
        <div class="wrapper wrapper-content" >
            <div class="container">
              <div class="row">
				<div class="col-md-2">
				</div>

                <div class="col-md-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Alteração de CERTIFICADO <small> </small></h5>
                        </div>
                        <div class="ibox-content">
                            <form  class="form-horizontal">
                                <input type="hidden" name="" value="" >

								<div class="form-group"><label class="col-sm-2 control-label">Id Os</label>

                                    <div class="col-sm-2"><input type="text" class="form-control" name="id_exp" id="id_exp"  required=""></div>
                                </div>
								<div class="form-group"><label class="col-sm-2 control-label">Id Chamado</label>

                                    <div class="col-sm-2"><input type="text" class="form-control" name="id_chamado" id="id_chamado" value="TSIS-" required=""></div>
                                </div>
                <div class="form-group"><label class="col-sm-2 control-label">Nº Certificado a alterar</label>

                                    <div class="col-sm-2"><input type="text" class="form-control" name="id_chamado" id="id_chamado" value="TSIS-" required=""></div>
                                </div>                                
								<div class="hr-line-dashed"></div>
                                <div class="form-group">
                                    <div class="col-sm-4">
                                        <button name="btn-exec" id="btn-exec" value="Listar" class="btn btn-gray" type="button" ><i class="fa fa-list"></i>&nbsp;Listar</button>
                                    </div>
                                </div>
                            </form>
                                
                                <div class="hr-line-dashed"  ></div>
                                  <div id="divAppend">
                                    <div style="overflow: auto;height:  300px" id="divList">
                                        
                                    </div>    
                                  </div>

                            <div class="hr-line-dashed"></div>

                        </div>
                    </div>
                </div>
            </div>

            </div>

        </div>


<script >
  {literal}


    $('document').ready(function(){   



    //execute SQL
    $('#btn-exec').on('click' ,function(){


        var url    = $('#base_url_list').val(); 


        var tr  = $(this),
          show    = tr.data('show'),
          hide    = tr.data('hide');

          var id_exp     = $("#id_exp").val();
          var id_chamado = $("#id_chamado").val(); 


          if(id_exp =="" || id_chamado =="" ){
            var msg = 'Os campos id do chamado e Oss são obrigatórios.';
            alertMsg(msg,'yellow');
            return;
          }


        $.jAlert({
              'title':'Executar?',
              'type':'confirm',
              'content':'Deseja realmente executar?',
                  'theme': 'blue',
                  'showAnimation' : show,
                  'hideAnimation' : hide,
                  'confirmBtnText': "Sim",
                  'denyBtnText': 'Não',
              'onConfirm': function() {



                    //---------------------------initialize AJAX GET
                    $.ajax({
                            cache:false,
                            type: 'POST',
                            url: url,
                            data: {id: id_exp,id_chamado:id_chamado},
                            dataType: "json",
                            success: function(data) {
                         

                         if(data.list.success){ 


                            $('#divList').remove();



                                $("#divAppend").append('<div id="divList" style="overflow: auto;height:  300px" ><table id="tableList"  class="table table-striped table-bordered table-sm" >'+
                                                '<thead id="the"><tr><td>id Fase Status</td><td>id Prestação Fase</td><td>Código Fase</td><td>Padrão</td><td>Prestação</td><td>Fase</td><td>Nome</td></tr>'+
                                                '</thead>'+
                                                '<tbody id="tbodyList" >'+
                                                '</tbody>'+
                                            '</table></div>')    

                                    var rows = "";
                                    var i=0;

                                    $.each(data.list.custom.query, function (i, item) {


                                            console.log(item);
                                            

                                            rows = rows + "<tr class='' >";
                                            rows = rows + "<td class='tdid' style='cursor:pointer;color:blue' data-id='"+item.id_fases_status +"'>"+item.id_fases_status+"</td>";
                                            rows = rows + "<td class='tdidlog"+i+"' data-idprestacaofase='"+item.id_prestacao_fase +"'>"+item.id_prestacao_fase+"</td>";
                                            rows = rows + "<td class='tdidlog"+i+"' data-idcodfase='"+item.cod_fase +"'>"+item.cod_fase+"</td>";
                                            rows = rows + "<td class='tdidlog"+i+"' data-idpadrao='"+item.padrao +"'>"+item.padrao+"</td>";
                                            rows = rows + "<td class='tdidlog"+i+"' data-idprestacao='"+item.prestacao +"'>"+item.prestacao+"</td>";
                                            rows = rows + "<td class='tdidlog"+i+"' data-idfase='"+item.fase +"'>"+item.fase+"</td>";
                                            rows = rows + "<td class='tdidlog"+i+"' data-idnome='"+item.nome +"'>"+item.nome+"</td>";
                                            rows = rows + "</tr>";


                                            i=i+1;                              
                                    });


                                        
                                        $("#tbodyList").html(rows);
                                        $('#tableList').DataTable();
                                        $('.dataTables_length').addClass('bs-select'); 
                                        $('.tdid').removeClass('sorting_1');

                                        //-----------------------------------show table
                               
                               }else{
                                  msg="não existe fase para ser alterada com essa Oss";  
                                  alertMsg(msg,'yellow');  
                                  $('#divList').remove();
                               }  

                            }
                    });
              },
            }); 
      
        });

    $(document).on('click', '.tdid', function() { 

      var url     = $('#base_url_add').val();
      var id      = $(this).data("id");
      var chamado = $("#id_chamado").val();
      var id_exp  = $("#id_exp").val();

        var tr  = $(this),
          show    = tr.data('show'),
          hide    = tr.data('hide');


        $.jAlert({
              'title':'Executar?',
              'type':'confirm',
              'content':'Deseja Alterar a Fase desse id (' +id+' )?',
                  'theme': 'blue',
                  'showAnimation' : show,
                  'hideAnimation' : hide,
                  'confirmBtnText': "Sim",
                  'denyBtnText': 'Não',
              'onConfirm': function() {


                    //---------------------------initialize AJAX GET
                    $.ajax({
                            type: 'POST',
                            url: url,
                            data: {id: id,id_chamado:chamado,id_exp: id_exp},
                            dataType: "json",
                            success: function(data) {
                              console.log(data)
                               if(data.success){
                                  var msg = data.message;
                                  alertMsg(msg,'green');
                               }else{
                                  var msg = data.message;
                                  alertMsg(msg,'red');
                               }


                            }
                    });
              },
            });      


    });
 
 });


    function alertMsg(msg,color){

        var title = "";

        if(color=="green"){
          title = "Sucesso";
        }else if(color=="yellow"){
          title="opsss!";
        }else if (color=="red"){
          title="Erro";
        }  
        

        var tr  = $(this),
          show    = tr.data('show'),
          hide    = tr.data('hide');

                $.jAlert({

                  'title':title,
                  'content':msg,
                      'theme': color,
                      'position':'600px',
                       
                      'showAnimation' : show,
                      'hideAnimation' : hide,
                       callback: function(value){ 
                                     // console.log(value); 
                               },
                      'btns': { 'text': 'Fechar' },       
                            'onOpen': function(alert){
                                window.setTimeout(function(){
                                    alert.closeAlert();
                                }, 3000);
                            }
                   });
                return;

    }


 {/literal}

</script>