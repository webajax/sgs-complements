<?php
/* Smarty version 3.1.30, created on 2019-04-24 16:30:32
  from "/var/www/html/sgs-complements/application/views/template/header.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5cc0b9583a9b20_43395049',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '24c2fd100c727d400bf2406564d5d9af9c97bee7' => 
    array (
      0 => '/var/www/html/sgs-complements/application/views/template/header.tpl',
      1 => 1556133053,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cc0b9583a9b20_43395049 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html>
<head>

 
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!--<meta http-equiv="content-type" content="text/html; charset=utf-8">-->
    <title>Sgs Tools</title>


  	<link href="<?php echo base_url('');?>
css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url('');?>
css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url('');?>
font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url('');?>
css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url('');?>
css/style.css" rel="stylesheet">    
    <link href="<?php echo base_url('');?>
css/basic.css" rel="stylesheet">     <!-- Usado pela area de upload de arquivos -->
    <link href="<?php echo base_url('');?>
css/dropzone.css" rel="stylesheet">  <!-- Usado pela area de upload de arquivos -->
    <link href="<?php echo base_url('');?>
css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="<?php echo base_url('');?>
css/plugins/chosen/chosen.css" rel="stylesheet">
    <link href="<?php echo base_url('');?>
css/plugins/colorpicker/bootstrap-colorpicker.min.css" rel="stylesheet">
    <link href="<?php echo base_url('');?>
css/plugins/cropper/cropper.min.css" rel="stylesheet">
    <link href="<?php echo base_url('');?>
css/plugins/switchery/switchery.css" rel="stylesheet">
    <link href="<?php echo base_url('');?>
css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url('');?>
css/plugins/nouslider/jquery.nouislider.css" rel="stylesheet">
    <link href="<?php echo base_url('');?>
css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    <link href="<?php echo base_url('');?>
css/plugins/ionRangeSlider/ion.rangeSlider.css" rel="stylesheet">
    <link href="<?php echo base_url('');?>
css/plugins/ionRangeSlider/ion.rangeSlider.skinFlat.css" rel="stylesheet">
 	<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <link href="<?php echo base_url('');?>
css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url('');?>
css/plugins/dataTables/dataTables.responsive.css" rel="stylesheet">
    <link href="<?php echo base_url('');?>
css/plugins/dataTables/dataTables.tableTools.min.css" rel="stylesheet">
	<link href="<?php echo base_url('');?>
css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
    <link href="<?php echo base_url('');?>
css/custom.css" rel="stylesheet">
    <link href="<?php echo base_url('');?>
css/select2.css" rel="stylesheet"> 

    <!--layers css-->
    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('');?>
css/jqcloud/jqcloud.css" />   


    <!--datepicker JS-->
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css" />
    <?php echo '<script'; ?>
 src="http://code.jquery.com/jquery-1.8.3.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"><?php echo '</script'; ?>
> 



    <!-- Mainly scripts -->
    <?php echo '<script'; ?>
 src="<?php echo base_url('');?>
js/jquery-2.1.1.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo base_url('');?>
js/bootstrap.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo base_url('');?>
js/plugins/metisMenu/jquery.metisMenu.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo base_url('');?>
js/plugins/slimscroll/jquery.slimscroll.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo base_url('');?>
js/plugins/jeditable/jquery.jeditable.js"><?php echo '</script'; ?>
>
    <!-- Custom and plugin javascript -->
    <?php echo '<script'; ?>
 src="<?php echo base_url('');?>
js/inspinia.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo base_url('');?>
js/plugins/pace/pace.min.js"><?php echo '</script'; ?>
>
    <!-- Flot -->
    <?php echo '<script'; ?>
 src="<?php echo base_url('');?>
js/plugins/flot/jquery.flot.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo base_url('');?>
js/plugins/flot/jquery.flot.tooltip.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo base_url('');?>
js/plugins/flot/jquery.flot.resize.js"><?php echo '</script'; ?>
>
    <!-- JSKnob -->
    <?php echo '<script'; ?>
 src="<?php echo base_url('');?>
js/plugins/jsKnob/jquery.knob.js"><?php echo '</script'; ?>
>
    <!-- ChartJS-->
    <?php echo '<script'; ?>
 src="<?php echo base_url('');?>
js/plugins/chartJs/Chart.min.js"><?php echo '</script'; ?>
>
    <!-- Peity -->
    <?php echo '<script'; ?>
 src="<?php echo base_url('');?>
js/plugins/peity/jquery.peity.min.js"><?php echo '</script'; ?>
>
    <!-- Peity demo -->
    <?php echo '<script'; ?>
 src="<?php echo base_url('');?>
js/demo/peity-demo.js"><?php echo '</script'; ?>
>
    <!-- Toggle script -->

    <?php echo '<script'; ?>
 src="<?php echo base_url('');?>
js/plugins/dataTables/jquery.dataTables.js"><?php echo '</script'; ?>
>

    <?php echo '<script'; ?>
 src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"><?php echo '</script'; ?>
>


    <!--select 2-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"><?php echo '</script'; ?>
>


    <!--jAlert-->
    <link href="<?php echo base_url('');?>
css/jAlert.css" rel="stylesheet"> 
    <?php echo '<script'; ?>
 type="text/javascript" src="<?php echo base_url('');?>
js/jAlert.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 type="text/javascript" src="<?php echo base_url('');?>
js/jAlert-functions.js"><?php echo '</script'; ?>
>



    <!--layers-->
    <?php echo '<script'; ?>
 type="text/javascript" src="<?php echo base_url('');?>
js/jqcloud/jqcloud-1.0.4.js"><?php echo '</script'; ?>
>


</head>
<body class="top-navigation">


    <div id="wrapper">
        <div id="page-wrapper" class="blue-bg">
          <div class="row border-bottom white-bg">
            <nav class="navbar navbar-fixed-top" role="navigation">
                <div class="navbar-header">
                    <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                        <i class="fa fa-reorder"></i>
                    </button>
                    <a href="<?php echo base_url('welcome');?>
" class="navbar-brand" text-font-bold="true">
                            <img src="<?php echo base_url('img/banner_sgs_logo_topo.jpg');?>
" style="position: relative;left: -50px" width="250px" height="40px">
                            </a>

                </div>
                <div class="navbar-collapse collapse" id="navbar" style="position: relative;left: -100px" >
                  <ul class="nav navbar-nav">
                    <li class="dropdown">
                      <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown"> Consultas <span class="caret"></span></a>
                      <ul role="menu" class="dropdown-menu">
                        <li>
                            <a href="<?php echo base_url('fase/show');?>
">Alteração de Fase</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('franquia/show');?>
">Alteração de Franquia %</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('analise/show');?>
">Exibir Botão Análise</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('status/show');?>
">Alterar Status para Aprovado</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('documento/show');?>
">Incluir Documento</a>
                        </li>
                      </ul>
                    </li>

                    <li class=" " >
                      <a aria-expanded="false" role="button" href="#"> Ajuda </a>
                    </li>
                    <li >


                    </li>
                  </ul>

                  <!--menu carga-->
 
                 
                  <!--end carga menu-->         

                    <ul class="nav navbar-top-links navbar-right">
                        <li>
                          <a href="<?php echo base_url('user/logout');?>
">
                            <i class="fa fa-sign-out"></i> Logout
                          </a>
                        </li>
                    </ul>
                </div>
            </nav>
          </div>

<?php }
}
