<?php

class Analise_service extends CI_Service{


	public function __construct(){

		parent::__construct();

		$this->load->model('analise_model');
		$this->load->helper('serviceresponse');
	}


	public function update($data){

		$resp = $this->analise_model->update($data);

		#if error...
		if(count($resp["query"])>0 and $resp["exception"]==null) {
           $resp = getServiceResponse(TRUE,"botão seguir análise exibido com sucesso!","",$resp);
		}else{
		 #case error goto roLlback or log
    	   $resp = getServiceResponse(FALSE,"Erro ao add  analise","erro",$resp);
		}

		return $resp;
	}


	public function find($id){

		$resp = $this->analise_model->find($id);

		#if error...
		if(count($resp["query"])>0 and $resp["exception"]==null) {
           $resp = getServiceResponse(TRUE,"Config listado com sucesso!","",$resp);
		}else if($resp["exception"]==null){
		 #case empty
    	   $resp = getServiceResponse(FALSE,"Não existe analise com essa Oss","erro",$resp);
		}else{
			#case error
      	   $resp = getServiceResponse(FALSE,"Erro ao listar analise","erro",$resp);
		}
		return $resp;


	}

	public function listAnalise($data){

		$resp = $this->analise_model->listanalise($data);


		#if error...
		if(count($resp["query"])>0 and $resp["exception"]==null) {
           $resp = getServiceResponse(TRUE,"analise listada com sucesso!","",$resp);
		}else if($resp["exception"]==null){
		 #case empty
    	   $resp = getServiceResponse(FALSE,"Essa Oss não tem botão análise para seguir","erro",$resp);
		}else{
			#case error
      	   $resp = getServiceResponse(FALSE,"Erro ao listar analise","erro",$resp);
		}

		
		return $resp;
	}




}	