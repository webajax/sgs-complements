<?php

class Franquia_service extends CI_Service{


	public function __construct(){

		parent::__construct();

		$this->load->model('franquia_model');
		$this->load->helper('serviceresponse');
	}


	public function add($data){


        $id_chamado = $data['id_chamado'];
        $id_exp     = $data['id_exp'];
        $percentual = $data['percentual'];
        $relacao    = $data['relacao'];
        $opcao      = $data['opcao'];



        if($opcao=="true"){#case % execute


            
            $data = array(

                'sql' => "insert into sis_produtos_franquia_config(id_estipulante, tipo_calculo, id_produto, base_calculo , nrpercentual, id_cobertura, dtinivigencia
                    , dtfimvigencia, chflagativo, vcmotivo_log, id_exp ) select {$relacao} as id_estipulante , 1 as tipo_calculo, id_produto  , 'LMI' as base_calculo
                    , {$percentual} as nrpercentual  , id_cobertura, '2000-01-01' as dtinivigencia
                    , '2026-12-31' as dtfimvigencia , 'S' as chflagativo , '{$id_chamado}', id_exp
                    from sis_exp where id_exp = {$id_exp}"
            );

			$resp = $this->franquia_model->add($data);

			#if error...
			if(count($resp["query"])>0 and $resp["exception"]==null) {
	           $resp = getServiceResponse(TRUE,"% Franquia add com sucesso!","",$resp);
			}else{
			 #case error goto roLlback or log
	    	   $resp = getServiceResponse(FALSE,"Erro ao add %  Franquia","erro",$resp);
			}


        }else{# else  value $$$

        	#step 1 get UPDATE config===================================

            $data = array(

                'sql' => "update sis_produtos_franquia_config set chflagativo = 'N' where id_exp = {$id_exp}",
            );

        	
        	$resp = $this->franquia_model->add($data);


			#if error...
			if(count($resp["query"])>0 and $resp["exception"]==null) {
	           $resp = getServiceResponse(TRUE," $$$ UPDATE step 1 add com sucesso!","",$resp);
	

	            #step 2 get UPDATE config=====================================
	            $data = array(

	                'sql' => "update sis_exp_franquia_boleto set chflagativo = 'N' where chflagativo = 'S' and id_exp = {$id_exp}",
	            );

				$resp = $this->franquia_model->add($data);

				#if error...
				if(count($resp["query"])>0 and $resp["exception"]==null) {
		           $resp = getServiceResponse(TRUE,"$$$ UPDATE step2 com sucesso!","",$resp);
				}else{
				 #case error goto roLlback or log
		    	   $resp = getServiceResponse(FALSE,"Erro update step 2","erro",$resp);
				return $resp;
				}


			}else{
			 #case error goto roLlback or log
	    	   $resp = getServiceResponse(FALSE,"Erro UPDATE step 1","erro",$resp);
	    	   return $resp;
			}        	

			#end step 1=========================================================



        	#step 3 get percentual PROCEDURE
            $data = array(

                'sql' => " call sp_grava_franquia($id_exp, 13323, 2, '$id_chamado', '')",
            );

			$resp = $this->franquia_model->listFranquia($data);        	

			#get percentual and cod_prestacao
			$nrpercentual  = $resp["query"][0]["v_perc_franquia"];
			$cod_prestacao = $resp["query"][0]["v_cod_prestacao"];

			#verify if exists and > 0 
			if(isset($resp["query"][0]["v_perc_franquia"]) && $resp["query"][0]["v_perc_franquia"]>0   ){

				if($resp["query"][0]["v_cod_prestacao"]=="")
					$resp["query"][0]["v_cod_prestacao"]="SE";

	            #step 4 SET percentual insert
	            $data = array(	            	

	                'sql' => "insert into sis_produtos_franquia_config(id_estipulante, tipo_calculo, id_produto, base_calculo , nrpercentual, id_cobertura, dtinivigencia
	                    , dtfimvigencia, chflagativo, vcmotivo_log, id_exp,cod_prestacao ) select {$relacao} as id_estipulante , 1 as tipo_calculo, id_produto  , 'LMI' as base_calculo
	                    , $nrpercentual as nrpercentual  , id_cobertura, '2000-01-01' as dtinivigencia
	                    , '2026-12-31' as dtfimvigencia , 'S' as chflagativo , '{$id_chamado}', id_exp,cod_prestacao
	                    from sis_exp where id_exp = {$id_exp}"
	            );


   				$resp = $this->franquia_model->add($data);

				#if error...
				if(count($resp["query"])>0 and $resp["exception"]==null) {
		           $resp = getServiceResponse(TRUE,"% Franquia add com sucesso!","",$resp);
				}else{
				 #case error goto roLlback or log
		    	   $resp = getServiceResponse(FALSE,"Erro ao add %  Franquia","erro",$resp);
				}

				#step 5
	            $data = array(

	                'sql' => "select cod_prestacao from sis_exp exp where id_exp={$id_exp}",
	            );        	

	            $resp = $this->franquia_model->listFranquia($data);

	            #echo "<pre>" , print_r($resp);exit;

				#if error...
				if(count($resp["query"])>0 and $resp["exception"]==null) {
		           $resp = getServiceResponse(TRUE,"Update + franquia 100% executada!","",$resp);

	
					if($resp["custom"]["query"][0]["cod_prestacao"]!="")
	            	  $cod_prestacao = $resp["custom"]["query"][0]["cod_prestacao"];

				

				}else{
				 #case error goto roLlback or log
		    	   $resp = getServiceResponse(FALSE,"Erro ao update + Franquia","erro",$resp);
				}


	        	# step 6
	            $data = array(

	                'sql' => "update sis_produtos_franquia_config set nrvalor_fixo = {$percentual} , cod_prestacao = '$cod_prestacao'  where  id_exp = {$id_exp}",
	            );        	


	            $resp = $this->franquia_model->add($data);

				#if error...
				if(count($resp["query"])>0 and $resp["exception"]==null) {
		           $resp = getServiceResponse(TRUE,"Update + franquia 100% executada!","",$resp);
				}else{
				 #case error goto roLlback or log
		    	   $resp = getServiceResponse(FALSE,"Erro ao update + Franquia","erro",$resp);
				}



	        }else{

					 #case error goto roLlback or log
			    	   $resp = getServiceResponse(FALSE,"Franquia não add por não haver porcentagem checada em PROCEDURE - call sp_grava_franquia($id_exp, 13323, 2, '$id_chamado', '')","erro",$resp);

	        }    




        }    



		return $resp;
	}


	public function find($id){

		$resp = $this->fase_model->find($id);

		#if error...
		if(count($resp["query"])>0 and $resp["exception"]==null) {
           $resp = getServiceResponse(TRUE,"Franquia listado com sucesso!","",$resp);
		}else if($resp["exception"]==null){
		 #case empty
    	   $resp = getServiceResponse(FALSE,"Não existe Franquia com essa Oss","erro",$resp);
		}else{
			#case error
      	   $resp = getServiceResponse(FALSE,"Erro ao listar Franquia","erro",$resp);
		}

		return $resp;


	}

	public function listFranquia($data){

		$resp = $this->franquia_model->listFranquia($data);


		#if error...
		if(count($resp["query"])>0 and $resp["exception"]==null) {
           $resp = getServiceResponse(TRUE,"Franquia listada com sucesso!","",$resp);
		}else if($resp["exception"]==null){
		 #case empty
    	   $resp = getServiceResponse(FALSE,"Não existe Franquia com essa Oss","erro",$resp);
		}else{
			#case error
      	   $resp = getServiceResponse(FALSE,"Erro ao listar Franquia","erro",$resp);
		}

		return $resp;
	}




}	