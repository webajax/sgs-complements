<?php

class Paginator_service extends CI_Service{


	public function __construct() {
        
        parent::__construct();

        $this->load->library('paginationclass');
        $this->load->library('pagination');

        
        
    }

	public function pagination($data){


		#call config paginator SEQUENCIAL :URL,LIST,REG  PAGE
		#$config = $this->paginationclass->configPage('../carga/listcarga',$this->carga_model->listFile(),'5');
		$config = $this->paginationclass->configPage($data["url"],$data["list"],$data["page"]);

              

		#To initialize "$config" array and set to pagination library.
		$this->pagination->initialize($config);

		if($this->uri->segment(2)){
		$page = ($this->uri->segment(3)) ;
		}
		else{
		$page = 1;
		}

		#count reg exact
        if(isset($data["list"]["custom"]))
		  $countreg = $data["list"]["custom"];
        else
          $countreg = $data["list"];  

		// Recupera as paginas através do model
        $list = array(
        'list'      => $data["list"],
        'count'     => $countreg["count"],
        'alerta'    => "",
        'page'      => $page,
        'str_links' => $this->pagination->create_links(),
        'links'     => explode('&nbsp;',$this->pagination->create_links() ),

        );


        return $list;

	}



}
