<?php

class Documento_service extends CI_Service{


	public function __construct(){

		parent::__construct();

		$this->load->model('documento_model');
		$this->load->helper('serviceresponse');
	}


	public function add($data){

		$resp = $this->fase_model->add($data);

		#if error...
		if(count($resp["query"])>0 and $resp["exception"]==null) {
           $resp = getServiceResponse(TRUE,"Fase add com sucesso!","",$resp);
		}else{
		 #case error goto roLlback or log
    	   $resp = getServiceResponse(FALSE,"Erro ao add  Fase","erro",$resp);
		}

		return $resp;
	}


	public function find($id){

		$resp = $this->fase_model->find($id);

		#if error...
		if(count($resp["query"])>0 and $resp["exception"]==null) {
           $resp = getServiceResponse(TRUE,"Config listado com sucesso!","",$resp);
		}else if($resp["exception"]==null){
		 #case empty
    	   $resp = getServiceResponse(FALSE,"Não existe Fase com essa Oss","erro",$resp);
		}else{
			#case error
      	   $resp = getServiceResponse(FALSE,"Erro ao listar Fase","erro",$resp);
		}
		return $resp;


	}

	public function listDocumento($data){

		$resp = $this->documento_model->listdocumento($data);


		#if error...
		if(count($resp["query"])>0 and $resp["exception"]==null and $resp["countreg"] > 0  ) {
           $resp = getServiceResponse(TRUE,"Documento listada com sucesso!","",$resp);
		}else if($resp["exception"]==null and  $resp["countreg"] > 0){
		 #case empty
    	   $resp = getServiceResponse(FALSE,"Não existe Documento com essa Oss","erro",$resp);
		}else if($resp["exception"]==null and  $resp["countreg"] == null){
			$resp = getServiceResponse(TRUE,"PARABÉNS ,Documento Add com sucesso!!!",$resp["countreg"],$resp);
		}else if($resp["exception"]!=null and  $resp["countreg"] == null){
			$resp = getServiceResponse(FALSE,"OOOPSSS, erro ao ADD doc!!!","",$resp);
		}else if($resp["exception"]!=null){
			#case error
      	   $resp = getServiceResponse(FALSE,"Erro ao listar Documento","erro",$resp);
		}

		return $resp;
	}




}	