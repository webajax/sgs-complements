<?php

class Status_service extends CI_Service{


	public function __construct(){

		parent::__construct();

		$this->load->model('status_model');
		$this->load->helper('serviceresponse');
	}


	public function update($data){

		$resp = $this->status_model->update($data);

		#if error...
		if(count($resp["query"])>0 and $resp["exception"]==null) {
           $resp = getServiceResponse(TRUE,"status aprovado alterado com sucesso!","",$resp);
		}else{
		 #case error goto roLlback or log
    	   $resp = getServiceResponse(FALSE,"Erro ao alterar status aprovado","erro",$resp);
		}

		return $resp;
	}


	public function find($id){

		$resp = $this->status_model->find($id);

		#if error...
		if(count($resp["query"])>0 and $resp["exception"]==null) {
           $resp = getServiceResponse(TRUE,"Config listado com sucesso!","",$resp);
		}else if($resp["exception"]==null){
		 #case empty
    	   $resp = getServiceResponse(FALSE,"Não existe analise com essa Oss","erro",$resp);
		}else{
			#case error
      	   $resp = getServiceResponse(FALSE,"Erro ao listar analise","erro",$resp);
		}
		return $resp;


	}

	public function listStatus($data){

		$resp = $this->status_model->liststatus($data);


		#if error...
		if(count($resp["query"])>0 and $resp["exception"]==null) {
           $resp = getServiceResponse(TRUE,"Oss status listada com sucesso!","",$resp);
		}else if($resp["exception"]==null){
		 #case empty
    	   $resp = getServiceResponse(FALSE,"Essa Oss já está com status de aprovada","erro",$resp);
		}else{
			#case error
      	   $resp = getServiceResponse(FALSE,"Erro ao listar status","erro",$resp);
		}

		
		return $resp;
	}




}	