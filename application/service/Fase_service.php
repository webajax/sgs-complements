<?php

class Fase_service extends CI_Service{


	public function __construct(){

		parent::__construct();

		$this->load->model('fase_model');
		$this->load->helper('serviceresponse');
	}


	public function add($data){

		$resp = $this->fase_model->add($data);

		#if error...
		if(count($resp["query"])>0 and $resp["exception"]==null) {
           $resp = getServiceResponse(TRUE,"Fase add com sucesso!","",$resp);
		}else{
		 #case error goto roLlback or log
    	   $resp = getServiceResponse(FALSE,"Erro ao add  Fase","erro",$resp);
		}

		return $resp;
	}


	public function find($id){

		$resp = $this->fase_model->find($id);

		#if error...
		if(count($resp["query"])>0 and $resp["exception"]==null) {
           $resp = getServiceResponse(TRUE,"Config listado com sucesso!","",$resp);
		}else if($resp["exception"]==null){
		 #case empty
    	   $resp = getServiceResponse(FALSE,"Não existe Fase com essa Oss","erro",$resp);
		}else{
			#case error
      	   $resp = getServiceResponse(FALSE,"Erro ao listar Fase","erro",$resp);
		}
		return $resp;


	}

	public function listFase($data){

		$resp = $this->fase_model->listfase($data);


		#if error...
		if(count($resp["query"])>0 and $resp["exception"]==null) {
           $resp = getServiceResponse(TRUE,"Fase listada com sucesso!","",$resp);
		}else if($resp["exception"]==null){
		 #case empty
    	   $resp = getServiceResponse(FALSE,"Não existe Fase com essa Oss","erro",$resp);
		}else{
			#case error
      	   $resp = getServiceResponse(FALSE,"Erro ao listar Fase","erro",$resp);
		}

		return $resp;
	}




}	