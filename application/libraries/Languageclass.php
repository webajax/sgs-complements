<?php
# Includes the autoloader for libraries installed with composer
defined('BASEPATH') OR exit('No direct script access allowed');
require './vendor/autoload.php';


# Imports the Google Cloud client library
use Google\Cloud\Language\LanguageClient;
putenv('GOOGLE_APPLICATION_CREDENTIALS=VERB-63a9e0c4b603.json'); //your path to file of cred




class Languageclass {


    private $CI;
    
    public function __construct() {
        #$this->CI =& get_instance();
        #$this->CI->load->library('session');

       
    }    
	
    public function analisaTexto($text){

    # Your Google Cloud Platform project ID
    //  $projectId = 'nluopen-178217';
	$projectId = 'utility-descent-186314';

	# Instantiates a client
    $language = new LanguageClient([
        'projectId' => $projectId
    ]);

    # Acessa Google e faz a anotacao do texto
    $annotation = $language->annotateText($text);

    //echo '<PRE>', print_r($annotation); exit;
            

    # Busca a análise de sentimento para o texto completo
    $docsentimentscore = $annotation->documentSentiment()['score'];

    $listsentencesdata = array();
    $listtokensdata    = array();
    $listentitiesname  = array();

    # Busca a análise lista de sentenças e o sentimento de cada uma delas
    foreach ($annotation->sentences() as $sentence) {
         $listsentencesdata[] = array(
           'textcontent' => $sentence['text']['content'],
           'beginoffset' => $sentence['text']['beginOffset'],
           'sentimentscore' => $sentence['sentiment']['score'],
           'magnitude' => $sentence['sentiment']['magnitude'],
       );
    }

    # Busca a lista de entidades
    foreach ($annotation->entities() as $entitie) {
         $listentitiesname[] = array(
           'name' => $entitie['name'],
           'text' => $entitie['mentions'][0]['text']['content'],
           'beginoffset' => $entitie['mentions'][0]['text']['beginOffset'],
       );
    }

    # Busca a lista de tokens
    $i=0;
    foreach ($annotation->tokens() as $token) {
        $listtokensdata[] = array(
           'textcontent' => $token['text']['content'],
           'beginoffset' => $token['text']['beginOffset'],
           'tag' => $token['partOfSpeech']['tag'],
           'position' => $i,
           'headTokenIndex'=> $token['dependencyEdge']['headTokenIndex'],
           'label' => $token['dependencyEdge']['label'],
           'lemma' => $token['lemma'],
       );
        $i++;
    }

    #return array sintaxes 
    $data = array(

        'docsentimentscore' => $docsentimentscore,
        'listsentencesdata' => $listsentencesdata,
        'listentitiesname'  => $listentitiesname,
        'listtokensdata'    => $listtokensdata,

    );


    return $data;
 
	}

}
