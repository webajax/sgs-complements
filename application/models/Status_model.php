<?php

class Status_model extends CI_Model {
    function __construct() {
		$this->load->database();
    }


    public function update($data){


       
       $response = $this->listStatus($data);

       if(isset($response["query"][0]["data_apr"])){
         $data_apr = $response["query"][0]["data_apr"];

              if($response){

                    try{
                     #cuidado ao mexer aki pois está em produção 
                     $query = $this->db->query("UPDATE sis_exp SET data_lib = '{$data_apr}'
                     where id_exp= {$data['id_exp']} ");   


                      $query = array(
                          'query'    =>$query,
                          'exception'=> null
                      );

                                      
                    }catch(Exception $e){

                      $errorInfo = array(
                          'query'    => 0,
                          'exception'=>$e->getMessage
                      );

                      return $errorInfo;
                    }

           }else{

            $query = array(
                'query'    =>$query,
                'exception'=> "Não existe data apr"
            );

           }
      }     

       return $query;

       
        
    }

public function del($id){

      try{  
      
        $query=$this->db->delete('vsis_vbt_carga_log', array('carga_id' => $id));

        $query = array(
            'query'    =>$query,
            'exception'=> null
        );


        return $query;
      
      }catch(Exception $e){

        $errorInfo = array(
            'query'    => 0,
            'exception'=>$e->getMessage
        );

        return $errorInfo;
      }

  }


    public function find($id){


      try{

        $query=$this->db->get_where('vsis_vbt_carga_log',array('carga_id'=>$id));

        $query = array(
            'query'    =>$query->result_array(),
            'exception'=> null
        );


        return $query;
      
      }catch(Exception $e){
        
        $errorInfo = array(
            'query'    => 0,
            'exception'=>$e->getMessage
        );

        return $errorInfo;
      }
    } 

    public function listStatus($data){

        $id_exp = $data["id_exp"];#get os

      try{

          $query=$this->db->query("select id_exp, data_apr, data_lib,reincidencia from sis_exp where id_exp =$id_exp" );



            $countreg = $query->num_rows();

            $query = array(
                'query'    =>$query->result_array(),
                'count'    => $countreg,
                'exception'=> null
            );


            return $query;
          
          }catch(Exception $e){
            
            $errorInfo = array(
                'query'    => 0,
                'exception'=>$e->getMessage
            );

            return $errorInfo;
          }


    }     

    

}