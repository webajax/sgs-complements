<?php

class Documento_model extends CI_Model {
    function __construct() {
		$this->load->database();
    }


    public function add($data){

      try{
        
        $query=$this->db->insert('sis_exp_fases_status', $data);

        $query = array(
            'query'    =>$query,
            'exception'=> null
        );

        return $query;
      
      }catch(Exception $e){

        $errorInfo = array(
            'query'    => 0,
            'exception'=>$e->getMessage
        );

        return $errorInfo;
      }
        
    }

public function del($id){

      try{  
      
        $query=$this->db->delete('vsis_vbt_carga_log', array('carga_id' => $id));

        $query = array(
            'query'    =>$query,
            'exception'=> null
        );


        return $query;
      
      }catch(Exception $e){

        $errorInfo = array(
            'query'    => 0,
            'exception'=>$e->getMessage
        );

        return $errorInfo;
      }

  }


    public function find($id){


      try{

        $query=$this->db->get_where('vsis_vbt_carga_log',array('carga_id'=>$id));

        $query = array(
            'query'    =>$query->result_array(),
            'exception'=> null
        );


        return $query;
      
      }catch(Exception $e){
        
        $errorInfo = array(
            'query'    => 0,
            'exception'=>$e->getMessage
        );

        return $errorInfo;
      }
    } 

    public function listDocumento($data){

        $id_exp = $data["id_exp"];#get os


      try{

          #first query  
          $query=$this->db->query("
            select
              e.id_contrato as id_contrato
            , e.id_cobertura as id_cobertura
            , e.id_cliente as id_cliente
            , e.cod_prestacao as cod_prestacao
            , e.cod_estipulante as cod_estipulante
            , r.id_relacao as id_relacao
              from sis_exp e, sis_clientes_estipulantes r
              where e.id_exp = {$id_exp} and r.cod_estipulante = e.cod_estipulante"
        );


            $countreg = $query->num_rows();

            $queryResult = array(
                'query'    =>$query->result_array(),
                'count'    => $countreg,
                'exception'=> null
            );


            #get result and insert second query
            $id_contrato  = $queryResult['query'][0]['id_contrato'];
            $id_cobertura = $queryResult['query'][0]['id_cobertura'];
            $id_prestacao = $queryResult['query'][0]['cod_prestacao'];
            $id_relacao   = $queryResult['query'][0]['id_relacao'];

          #second query
          $query=$this->db->query("
             select * FROM sis_clientes_estipulantes_documentos ced
             WHERE 1=1
             and ced.id_contrato        = $id_contrato
             and ced.id_cobertura       = $id_cobertura
             and ced.cod_prestacao      = '$id_prestacao'
             and ced.id_relacao_cli_est = $id_relacao
             and desativado='0' "
          );

            $countreg = $query->num_rows();

            $query = array(
                'query'    =>$query->result_array(),
                'count'    => $countreg,
                'exception'=> null
            );

            echo "<pre>" ,print_r($query);exit;

            #case exists reg $query insert doc
            if($countreg>0){

              $query=$this->db->query("
                 INSERT INTO sis_exp_documentos 
                (`id_exp`, `id_cli_est_doc`, `solicitado`, `data_prev_recebimento`, `analisado`, `folha_unica`, `solicitado_por`, `ordem`, `ativo`) 
                SELECT e.id_exp, id_cli_est_doc, '1', date(adddate(now(), interval 7 day)), 'N', 'N', 'S', '1', 'S'
                from sis_exp e
                LEFT join sis_clientes_estipulantes ce on e.cod_estipulante = ce.cod_estipulante
                left JOIN sis_clientes_estipulantes_documentos ced on ced.id_relacao_cli_est = ce.id_relacao
                LEFT join sis_tipos_documentos td on td.id_tipos_documentos=ced.id_tipo_documento
                where
                ced.id_relacao_cli_est=ce.id_relacao
                and ce.id_cliente=e.id_cliente
                and ced.id_contrato={$id_contrato}
                and ced.id_cobertura= {$id_cobertura}
                and desativado='0'
                and ced.cod_prestacao=e.cod_prestacao
                and ced.obrigatorio = '1'
                AND if(e.id_segurado =0,ced.solicitacao in ('N','T'),ced.solicitacao in ('T','S') )
                AND e.id_exp in(
                ${id_exp}
                )
                group by ced.id_tipo_documento;                
                "
              );                


             $query = array(
                'query'     =>$query,
                'exception' => null,
                'countreg'  => null
             );


            }

            return $query;
          
          }catch(Exception $e){
            
            $errorInfo = array(
                'query'    => 0,
                'exception'=>$e->getMessage
            );

            return $errorInfo;
          }


    }     

    

}