<?php

class Analise_model extends CI_Model {
    function __construct() {
		$this->load->database();
    }


    public function update($data){

      $id_exp = $data["id_exp"];

      try{

        
        
        $responseSE = $this->se($id_exp);


        if($responseSE["count"]==0)
             $query=$this->db->query("select id_exp_complemento,depreciacao_tempo as cod_prestacao, data_apr_rs as data_apr from sis_exp_complemento where id_exp = {$id_exp} ");


        if($responseSE["count"]>0)
          $sql = "UPDATE sis_exp set data_apr_se = NULL where id_exp = {$id_exp}";#substituicao
        else
          $sql="UPDATE  sis_exp_complemento set data_apr_rs = NULL  where id_exp = {$id_exp}";#ressarcimento


       #cuidado ao mexer aki pois está em produção 
       $query = $this->db->query($sql);   


        $query = array(
            'query'    =>$query,
            'exception'=> null
        );

        return $query;
      
      }catch(Exception $e){

        $errorInfo = array(
            'query'    => 0,
            'exception'=>$e->getMessage
        );

        return $errorInfo;
      }
        
    }

    private function se($id_exp){

      try{


            $query=$this->db->query("select id_exp, cod_prestacao, data_apr_se as data_apr from sis_exp where id_exp =  {$id_exp} AND cod_prestacao='SE' ");

              $countreg = $query->num_rows();

              $query = array(
                  'query'    =>$query->result_array(),
                  'count'    => $countreg,
                  'exception'=> null
              );


              return $query;
            
            }catch(Exception $e){
              
              $errorInfo = array(
                  'query'    => 0,
                  'exception'=>$e->getMessage
              );

              return $errorInfo;
            }

    }


public function del($id){

      try{  
      
        $query=$this->db->delete('vsis_vbt_carga_log', array('carga_id' => $id));

        $query = array(
            'query'    =>$query,
            'exception'=> null
        );


        return $query;
      
      }catch(Exception $e){

        $errorInfo = array(
            'query'    => 0,
            'exception'=>$e->getMessage
        );

        return $errorInfo;
      }

  }


    public function find($id){


      try{

        $query=$this->db->get_where('vsis_vbt_carga_log',array('carga_id'=>$id));

        $query = array(
            'query'    =>$query->result_array(),
            'exception'=> null
        );


        return $query;
      
      }catch(Exception $e){
        
        $errorInfo = array(
            'query'    => 0,
            'exception'=>$e->getMessage
        );

        return $errorInfo;
      }
    } 

    public function listAnalise($data){

        $id_exp = $data["id_exp"];#get os

      try{

          #veRirfy SE 
          $query = $this->se($id_exp);


          if($query["count"]==0){
             $query=$this->db->query("select id_exp_complemento as id_exp ,depreciacao_tempo as cod_prestacao, data_apr_rs as data_apr from sis_exp_complemento where id_exp = {$id_exp} ");

            $countreg = $query->num_rows();

            $query = array(
                'query'    =>$query->result_array(),
                'count'    => $countreg,
                'exception'=> null
            );


            return $query;

           }else{

            $countreg = $query["count"];

            $query = array(
                'query'    =>$query,
                'count'    => $countreg,
                'exception'=> null
            );


            return $query["query"];
           }







          
          }catch(Exception $e){
            
            $errorInfo = array(
                'query'    => 0,
                'exception'=>$e->getMessage
            );

            return $errorInfo;
          }


    }     

    

}