<?php

class fase_model extends CI_Model {
    function __construct() {
		$this->load->database();
    }


    public function add($data){

      try{
        
        $query=$this->db->insert('sis_exp_fases_status', $data);

        $query = array(
            'query'    =>$query,
            'exception'=> null
        );

        return $query;
      
      }catch(Exception $e){

        $errorInfo = array(
            'query'    => 0,
            'exception'=>$e->getMessage
        );

        return $errorInfo;
      }
        
    }

public function del($id){

      try{  
      
        $query=$this->db->delete('vsis_vbt_carga_log', array('carga_id' => $id));

        $query = array(
            'query'    =>$query,
            'exception'=> null
        );


        return $query;
      
      }catch(Exception $e){

        $errorInfo = array(
            'query'    => 0,
            'exception'=>$e->getMessage
        );

        return $errorInfo;
      }

  }


    public function find($id){


      try{

        $query=$this->db->get_where('vsis_vbt_carga_log',array('carga_id'=>$id));

        $query = array(
            'query'    =>$query->result_array(),
            'exception'=> null
        );


        return $query;
      
      }catch(Exception $e){
        
        $errorInfo = array(
            'query'    => 0,
            'exception'=>$e->getMessage
        );

        return $errorInfo;
      }
    } 

    public function listFase($data){

        $id_exp = $data["id_exp"];#get os

      try{

          $query=$this->db->query("
            SELECT
                fs.id_fases_status,
                fs.id_prestacao_fase,
                pf.cod_fase,
                fs.padrao,
                prt.prestacao,
                pf.descricao AS fase,
                s.nome
            FROM
                sis_fases_status AS fs
                    JOIN
                sis_prestacao_fase AS pf ON pf.id_prestacao_fase = fs.id_prestacao_fase
                    JOIN
                sis_prestacoes AS prt ON prt.id_prestacao = pf.id_prestacao
                    JOIN
                sis_status AS s ON fs.id_status = s.id_status
            WHERE
                1 = 1
                    AND prt.cod_prestacao = (SELECT
                        cod_prestacao
                    FROM
                        sis_exp
                    WHERE
                        id_exp = '$id_exp')
                    AND fs.status = 'S'"
        );

            $countreg = $query->num_rows();

            $query = array(
                'query'    =>$query->result_array(),
                'count'    => $countreg,
                'exception'=> null
            );


            return $query;
          
          }catch(Exception $e){
            
            $errorInfo = array(
                'query'    => 0,
                'exception'=>$e->getMessage
            );

            return $errorInfo;
          }


    }     

    

}