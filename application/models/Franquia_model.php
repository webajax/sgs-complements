<?php

class franquia_model extends CI_Model {
    function __construct() {
		$this->load->database();
    }


    public function add($data){

      try{

        
        $query=$this->db->query($data["sql"]);

        $query = array(
            'query'    =>$query,
            'exception'=> null
        );

        return $query;
      
      }catch(Exception $e){

        $errorInfo = array(
            'query'    => 0,
            'exception'=>$e->getMessage
        );

        return $errorInfo;
      }
        
    }

public function del($id){

      try{  
      
        $query=$this->db->delete('vsis_vbt_carga_log', array('carga_id' => $id));

        $query = array(
            'query'    =>$query,
            'exception'=> null
        );


        return $query;
      
      }catch(Exception $e){

        $errorInfo = array(
            'query'    => 0,
            'exception'=>$e->getMessage
        );

        return $errorInfo;
      }

  }


    public function find($id){


      try{

        $query=$this->db->get_where('vsis_vbt_carga_log',array('carga_id'=>$id));

        $query = array(
            'query'    =>$query->result_array(),
            'exception'=> null
        );



        return $query;
      
      }catch(Exception $e){
        
        $errorInfo = array(
            'query'    => 0,
            'exception'=>$e->getMessage
        );

        return $errorInfo;
      }
    } 

    public function listFranquia($data){

      try{

          $query_result=$this->db->query($data["sql"]);



            $countreg = $query_result->num_rows();

            $query_result = array(
                'query'    =>$query_result->result_array(),
                'count'    => $countreg,
                'exception'=> null
            );
              

            $this->db->close();


            return $query_result;
          
          }catch(Exception $e){
            
            $errorInfo = array(
                'query'    => 0,
                'exception'=>$e->getMessage
            );

            return $errorInfo;
          }


    }     

    

}